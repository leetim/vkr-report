# Пример отчета в Latex

Пример отчета latex

## Links

- https://www.tandfonline.com/doi/full/10.1080/02564602.2018.1450649
- https://arxiv.org/pdf/1907.12219.pdf
- https://link.springer.com/chapter/10.1007/978-981-13-6861-5_47
- https://pdfs.semanticscholar.org/f714/f59aa29720d0cd08e9cae50706c32f9f1f12.pdf
- https://www.secen.org/wp-content/uploads/2019/01/final_conference_proceedings.pdf#page=72
- https://link.springer.com/content/pdf/10.1007%2F11427445_24.pdf
